<?php
include_once 'vendor/autoload.php';

use Pondit\Calculator\VolumeCalculator\Volume;
use Pondit\Calculator\VolumeCalculator\Cube;
use Pondit\Calculator\VolumeCalculator\Cone;
use Pondit\Calculator\VolumeCalculator\Cylinder;

$volume=new Volume();
var_dump($volume);

$cube=new Cube();
var_dump($cube);

$cone=new Cone();
var_dump($cone);

$cylinder=new Cylinder();
var_dump($cylinder);

